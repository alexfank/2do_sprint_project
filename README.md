# 4to_sprint_project

API de gestión de pedidos del restairant Delilah Restó.

El proyecto se encuentra instalado y corriendo en una instancia de AWS bajo el dominio www.alexfank.tk

Para evaluar se ha enviado credenciales para que un Tech Reviewer pueda calificar el proyecto.

Continúa operativo el Swagger para probar los endpoints desde la siguiente url: https://www.alexfank.tk/api-docs

## Instalación local

Creamos una copia del proyecto.

```bash
git clone https://gitlab.com/alexfank/2do_sprint_project
```

Instalar un gestor de base de datos MySQL como XAMPP o MAMP, asignando como usuario y contraseña "root" y puerto 3306. Luego crear una base de datos que se llame "database2doSP"

Utilizamos el administrador de paquetes de Node.

```bash
npm install
```
## Instalación a través de DockerFile

```bash
git clone https://gitlab.com/alexfank/2do_sprint_project
```

Montamos la imagen en Docker:
```bash
docker build -t delilahresto .
```

Ejecutamos el contenedor en Docker:
```bash
docker run -d --name delilah-resto -p 3000:3000 delilahresto
```
## Instalación a través de Docker-Compose

```bash
git clone https://gitlab.com/alexfank/2do_sprint_project
```

Montamos la imagen en Docker:
```bash
docker build -t delilahresto .
```

Ejecutamos el Docker-Compose:
```bash
docker-compose up
```

Esto ejecutará contenedores para la app a través de Node, Redis para caché, MySQL para la DB y phpMyAdmin para poder visualizar la base de datos en el puerto 8080. 

## Ejecución

Para iniciar el servidor escribimos en el terminal "npm run dev" y para correr el test en una segunda ventana de terminal escribimos "npm run test"
Para chequear la documentación de Swagger, en un navegador ingresar la siguiente url: http://localhost:3000/api-docs

Ya existe una carga bulk de datos de ejemplo.

Para loguearse como administrador usar "admin" como usuario y contraseña.

## Credenciales para compra de ejemplo en Mercado Pago

Nombre: Martinez Mirtha

Email: test_user_39575083@testuser.com

DNI: 23011111114

Número de tarjeta: 5031 7557 3453 0604

Código de seguridad: 123

Expiración: 11/25

## Usamos

```javascript
    chai 4.3.4,
    chai-http 4.3.0,
    cors 2.8.5,
    dotenv 10.0.0,
    express 4.17.1,
    express-openid-connect 2.7.1,
    helmet 4.6.0,
    jsonwebtoken 8.5.1,
    mercadopago 1.5.12,
    mocha 9.1.3,
    mysql2 2.3.0,
    nodemon 2.0.12,
    passport 0.5.2,
    passport-facebook 3.0.0,
    passport-github 1.1.0,
    passport-google-oauth20 2.0.0,
    passport-google-oidc 0.1.0,
    passport-linkedin-oauth2 2.0.0,
    redis 3.1.2,
    sequelize 6.6.5,
    swagger-jsdoc 6.1.0,
    swagger-ui-express 4.1.6,
    yamljs 0.3.0
```
## Alumno / Desarrollador
Alex Fank
