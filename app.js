const express = require('express');
const app = express();
const helmet = require('helmet');
const cors = require('cors')
const passport = require('passport');
const initServices = require('./src/services');
const prepareRoutes = require('./src/routes/auth');
const preparePaymentRoutes = require('./src/routes/payment');
const initPublicRouter = require('./src/routes/public');
const { initializeAuth0 } = require('./src/services/auth0');

//ASOCIACIONES
const associations = require('./src/database/associations');

//LLAMO AL ARCHIVO CONFIG
const { server } = require('./src/config/');

//MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Add headers before the routes are defined
app.use(cors());
initServices(app);

// Initializes passport and passport sessions
app.use(passport.initialize());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());
app.use(prepareRoutes());
app.use(preparePaymentRoutes());

//AUTH0 & PUBLIC ROUTES
initializeAuth0(app);
app.use(initPublicRouter());

//MIDDLEWARE HELMET
//app.use(helmet())
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);

//CONFIGURAR SWAGGER
const YAML = require('yamljs');
const swaggerJsDoc = YAML.load('./swagger.yaml');
const swaggerUI = require('swagger-ui-express');
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerJsDoc));

//LLAMADA A DB
require('./src/database/db');
require('./src/database/sync');

//CONFIGURACIÓN DE RUTAS
const apiRouter = require('./src/routes/api.js');
app.use('/api', apiRouter);

//LLAMO A CARPETA PUBLIC PARA FRONTEND
app.use(express.static('./public'));

//HANDLER DE ERRORES
app.use((err, req, res, next) => {
  if (err) {
    console.log(err);
    return res.status(500).send('Ha ocurrido un error')
  }
  next();
});

app.listen(server.port, () => {
  console.log(`Servidor iniciado en el puerto ${server.port}`)
});
