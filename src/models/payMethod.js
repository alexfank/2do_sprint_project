module.exports = (sequelize, type) => {
    return sequelize.define(
        "PaymentMethod",
        {
            name: type.STRING,
        },
        {
            sequelize,
            timestamps: false,
        }
    );
};