module.exports = (sequelize, type) => {
    return sequelize.define(
        "Products",
        {
            name: type.STRING,
            price: type.INTEGER,
            available: {
                type: type.BOOLEAN,
                defaultValue: 1
            }, 
        },
        {
            sequelize,
            timestamps: false,
        }
    );
};