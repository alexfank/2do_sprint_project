module.exports = (sequelize, type) => {
    const OrderDetails = sequelize.define(
        "OrderDetails",
        {
            productID: type.INTEGER,
            qty: type.INTEGER,
            subTotal: type.FLOAT
        },
        {
            sequelize,
            timestamps: true,
        }
    );

    return OrderDetails;
};