const { database2doSP } = require("../database/db");

module.exports = (sequelize, type) => {
    const Users = sequelize.define(
        "Users",
        {
            user: type.STRING,
            password: type.STRING,
            token: type.STRING(1000),
            name: type.STRING,
            email: {
                type: type.STRING,
                validate: {
                    isEmail: {
                        args: true,
                        msg: 'Por favor ingresar un email válido (***@***.com)'
                    }
                }
            },
            phone: type.INTEGER(20),
            isAdmin: {
                type: type.BOOLEAN,
                defaultValue: 0
            }, 
            isLogged: {
                type: type.BOOLEAN,
                defaultValue: 0
            },
            banned: {
                type: type.BOOLEAN,
                defaultValue: 0
            }
        },
        {
            sequelize,
            timestamps: false,
        }
    );

    return Users;
};