module.exports = (sequelize, type) => {
    const Orders = sequelize.define(
        "Orders",
        {
            user: type.STRING,
            status: {
                type: type.STRING,
                defaultValue: 'Pendiente'
            },
            address: type.STRING(50),
            total: type.FLOAT,
            payID: type.INTEGER,
        },
        {
            sequelize,
            timestamps: true,
        }
    );

    return Orders;
};