module.exports = (sequelize, type) => {
    const Addresses = sequelize.define(
        "Addresses",
        {
            address: type.STRING,
        },
        {
            sequelize,
            timestamps: false,
        }
    );

    return Addresses;
};