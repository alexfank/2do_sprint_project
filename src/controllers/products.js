const { Products } = require('../database/sync')
const { client } = require('../services/cache')

//LISTAR TODOS LOS PRODUCTOS (Pruebo si están en caché primero)

const allProducts = async (req, res) => {
    client.get('products', async (err, products) => {
        if(err) {
            res.status(400).send(err);
        }
        if(products) {
            console.log('Lista de productos enviada por caché',products)
            res.status(200).json({
                msg: 'Lista de productos',
                productos: {products}
            })
        }else{
            console.log('No hay información en caché de la lista de productos')
            
            const products = await Products.findAll();

            client.setex('products', 60 * 60, JSON.stringify(products), (error) => {
                if(error){
                    console.log('No se pudo guardar la lista en caché', error)
                    res.status(400).send('No se ha podido enviar la lista de productos')
                }
                res.status(202).json({
                    msg: 'Lista de productos',
                    items: { products }
                })
            })
        }
    });
};

//CREAR PRODUCTO
const newProduct = async (req, res) => {
    const { name, price } = req.body;
    const new_product = await Products.create({
        name: name,
        price: price
    });
    console.log('Producto creado exitosamente');
    //ACTUALIZO CACHE
    const products = await Products.findAll();
    client.setex('products', 60 * 60, JSON.stringify(products), (error) => {
        if(error){
            console.log('No se pudo actualizar la lista en caché', error)
        }else{
            console.log('Lista de productos en caché actualizada')
        }
    });
    //FIN DE ACTUALIZACIÓN DE CACHE
    res.status(201).json({
        title: 'Producto creado',
        product_data: new_product
    });
};

//MODIFICAR PRODUCTO
const modProduct = async (req, res) => {
    const { name, price, available } = req.body;
    const mod_product = await Products.update({
        name: name,
        price: price,
        available: available
    }, {
        where: {
            id: req.params.id
        }
    });
    //ACTUALIZO CACHE
    const products = await Products.findAll();
    client.setex('products', 60 * 60, JSON.stringify(products), (error) => {
        if(error){
            console.log('No se pudo actualizar la lista en caché', error)
        }else{
            console.log('Lista de productos en caché actualizada')
        }
    });
    //FIN DE ACTUALIZACIÓN DE CACHE
    res.status(201).json({
        title: 'Producto modificado correctamente',
        product_data: { name, price, available }
    });
};

//ELIMINAR PRODUCTO
const delProduct = async (req, res) => {
    const product = await Products.destroy({
        where: {
            id: req.params.id,
        }
    })
    res.status(201).json({
        title: 'Producto eliminado correctamente'
    });
};

module.exports = {
    allProducts,
    newProduct,
    modProduct,
    delProduct
};