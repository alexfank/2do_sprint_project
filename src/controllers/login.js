const { Users, Addresses } = require('../database/sync');
const jwt = require('jsonwebtoken');
const config = require('../config');
const { isLogged } = require('../middleware/users');
const { client } = require('../services/cache')

//LOGIN USUARIO
const login = async (req, res) => {
    try {
        await Users.findOne({
            where: {
                user: req.body.user
            }
        })
        .then(loginData => {
            if (loginData.password == req.body.password) {
                //Controla si esta banneado
                if(loginData.banned == 1){
                    return res.status(401).json({
                        title: 'El usuario se encuentra suspendido y no podrá loguearse.',
                    })
                }
                //Desloguea todos los usuarios
                const logOutAll = Users.update({
                    isLogged: '0',
                    token: null
                },{
                    where: {
                        isLogged: '1'
                    }
                });
                //Loguea al usuario con token
                let token = jwt.sign({ user: loginData }, config.jwt.secret, {
                    expiresIn: config.jwt.expires
                });
                Users.update({
                    token: token,
                    isLogged: '1'
                }, {
                    where: {
                        user: loginData.user
                    }
                })
                loginData.token = token;
                //GUARDA TOKEN DEL USUARIO EN CACHE
                client.setex('user_token', 60 * 60, token, (error) => {
                    if(error){
                        console.log('No se pudo guardar el token en caché', error)
                    }else{
                        console.log('Token del usuario logueado guardado en caché', token)
                    }
                })
                //FIN DE GUARDAR TOKEN DEL USUARIO EN CACHE
                res.status(200).json({
                    title: 'Usuario logueado correctamente',
                    data: loginData
                })
            } else {
                res.status(401).json({
                    title: 'La contraseña es incorrecta'
                })
            }
        })
    } catch (err) {
        res.status(404).json({
            title: 'El usuario no existe',
        })
    }
};

//CREAR USUARIO
const newUser = async (req, res) => {
    try {
        const { user, password, name, phone, address, email } = req.body;
        const new_user = await Users.create({
            user: user,
            password: password,
            name: name,
            email: email,
            phone: phone,
        })
            .then(user => {
                console.log(user.id)
                Addresses.create({
                    address: address,
                    UserId: user.id
                })
            })

        console.log('Usuario creado exitosamente');
        res.status(201).json({
            title: 'Usuario creado',
            user_data: { user, name, phone, address, email }
        });
    } catch (err) {
        res.status(400).json({
            msg: 'Ha ocurrido un error',
            error: err
        })
    }
};

module.exports = {
    newUser,
    login,
}