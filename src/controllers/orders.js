const { database2doSP } = require('../database/db');
const { Orders, OrderDetails, Addresses, Products, Users } = require('../database/sync');

//LISTAR TODAS LAS ORDENES
const allOrders = async (req, res) => {
    const orders = await Orders.findAll({
        include: [OrderDetails]
    });
    res.status(201).json({
        title: 'Listado de todas las ordenes',
        items: orders
    });
};

//CREAR ORDEN
const newOrder = async (req, res) => {
    const { productID, qty, address, payID } = req.body;
    let product, new_order;
    await Users.findOne({
        where: {
            isLogged: 1
        }
    })
        .then(async user => {
            const findAddress = await Addresses.findAll({
                where: {
                    UserId: user.id,
                }
            })
            findAddress.forEach(i => {
                if (i.address == address) {
                    userAddress = address;
                } else {
                    if(!address){
                        userAddress = findAddress[0].address
                    }else{
                        Addresses.create({
                            address: address,
                            UserId: user.id
                        })
                    }
                }
            })
            new_order = await Orders.create({
                user: user.user,
                address: userAddress,
                payID: payID
            })
        }).then(async data => {
            product = await Products.findOne({
                where: {
                    id: productID
                }
            })
            if (!product) {
                res.status(400).json({
                    msg: 'El producto no existe'
                })
            } else {
                const detail = await OrderDetails.create({
                    OrderId: new_order.id,
                    productID: productID,
                    qty: qty,
                    subTotal: product.price * qty
                })
            }
        })
        .then(async () => {
            const orderSearch = await OrderDetails.findAll({
                where: {
                    OrderId: new_order.id,
                }
            })
            let total = 0;
            orderSearch.forEach(i => {
                total += i.subTotal;
            })
            await Orders.update({
                total: total,
            }, {
                where: {
                    id: new_order.id
                }
            })
        })
        .then(() => {
            res.status(200).json({
                msg: 'Orden creada correctamente',
                order_data: { productID, qty, userAddress, payID }
            })
        }).catch(err=>{
            res.status(400).json({
                msg: 'Ha ocurrido un error',
                error: err
            })
        })
};

//AGREGAR A LA ORDEN
const addToOrder = async (req, res) => {
    const { productID, qty } = req.body;
    const orderID = req.params.id;
    await Products.findOne({
        where: {
            id: productID
        }
    })
    .then(async product => {
        if(product){
            await OrderDetails.create({
                productID: productID,
                OrderId: orderID,
                qty: qty,
                subTotal: product.price * qty
            })
        }else{
            return res.status(400).json({
                msg: 'El ID de producto no existe'
            })
        }
    })
    .then(async () => {
        const orderSearch = await OrderDetails.findAll({
            where: {
                OrderId: orderID,
            }
        })
        let total = 0;
        orderSearch.forEach(i => {
            total += i.subTotal;
        })
        await Orders.update({
            total: total,
        }, {
            where: {
                id: orderID
            }
        })
    })
    .then(
        res.status(201).json({
            msg: 'Items agregados a la orden correctamente',
            data: { orderID, productID, qty }
        }))
    .catch(err => {
        return res.status(400).json({
            msg: 'Ocurrió un error',
            error: err
        })
    })
};

//MODIFICAR ESTADO DE LA ORDEN
const modOrder = async (req, res) => {
    await Orders.update({
        status: req.body.status,
    }, {
        where: {
            id: req.params.id,
        }
    })
        .then(
            res.status(201).json({
                title: 'Orden modificada correctamente',
            }))
        .catch(err => {
            res.status(400).json({
                title: 'Ocurrió un error',
                error: err
            })
        })
};

//ELIMINAR ORDEN
const delOrder = async (req, res) => {
    await Orders.destroy({
        where: {
            id: req.params.id,
        }
    }).then(
        res.status(201).json({
            title: `Orden ${req.params.id} eliminada correctamente`,
        })
    ).catch(err => {
        res.status(400).json({
            title: 'Ocurrió un error',
            error: err
        })
    })
};

//QUITAR DE LA ORDEN
const delFromOrder = async (req, res) => {
    const datata = await OrderDetails.destroy({
        where: {
            OrderId: req.params.id,
            productID: req.body.productID
        }
    })
    .then(async data => {

            const orderSearch = await OrderDetails.findAll({
                where: {
                    OrderId: req.params.id,
                }
            })
            let total = 0;
            orderSearch.forEach(i => {
                total += i.subTotal;
            })
            await Orders.update({
                total: total,
            }, {
                where: {
                    id: req.params.id
                }
            })
    })
    .then(
        res.status(201).json({
            msg: 'Item eliminado de la orden correctamente'
        }))
    .catch(err => {
        return res.status(400).json({
            msg: 'Ocurrió un error',
            error: err
        })
    })
};

module.exports = {
    allOrders,
    newOrder,
    addToOrder,
    modOrder,
    delOrder,
    delFromOrder
};