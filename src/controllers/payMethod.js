const { PayMethods } = require('../database/sync')

//LISTAR TODOS LOS MEDIOS DE PAGO
const allPayMeth = async (req, res) => {
    await PayMethods.findAll()
        .then(data =>
            res.status(201).json({
                title: 'Listado de medios de pago',
                items: data
            }))
        .catch(err => {
            send.status(400).json({
                msg: 'Ha ocurrido un error',
                error: err
            })
        })
};

//CREAR MEDIO DE PAGO
const newPayMeth = async (req, res) => {
    await PayMethods.create({
        name: req.body.name,
    }).then(data =>
        res.status(201).json({
            title: 'Producto creado',
            product_data: data
        })
    ).catch(err => {
        send.status(400).json({
            msg: 'Ha ocurrido un error',
            error: err
        })
    })
};

//MODIFICAR MEDIO DE PAGO
const modPayMeth = async (req, res) => {
    await PayMethods.update({
        name: req.body.name,
    }, {
        where: {
            id: req.params.id
        }
    }).then(data =>
        res.status(201).json({
            title: 'Método de pago modificado correctamente',
            id: req.params.id,
            name: req.body.name
        })
    ).catch(err => {
        send.status(400).json({
            msg: 'Ha ocurrido un error',
            error: err
        })
    })
};

//ELIMINAR MEDIO DE PAGO
const delPayMeth = async (req, res) => {
    await PayMethods.destroy({
        where: {
            id: req.params.id,
        }
    }).then(data =>
        res.status(201).json({
            title: 'Método de pago eliminado correctamente'
        })
    ).catch(err => {
        send.status(400).json({
            msg: 'Ha ocurrido un error',
            error: err
        })
    })
};

module.exports = {
    allPayMeth,
    newPayMeth,
    modPayMeth,
    delPayMeth
};