const { database2doSP } = require('../database/db');
const { Users, Addresses } = require('../database/sync')

//LISTAR TODOS LOS USUARIOS
const allUsers = async (req, res) => {
    const users = await Users.findAll({
        include: 'Addresses'
    }).then(data => {
        res.status(201).json({
            title: 'Listado de usuarios',
            users_data: data
        })
    })
        .catch(err => {
            res.status(400).json({
                title: 'Ocurrió un error',
                error: err
            })
        })
};

//LISTAR UN USUARIO POR ID
const getUser = async (req, res) => {
    await Users.findByPk(req.params.id)
        .then(result => {
            if (result) {
                res.status(201).json({
                    title: 'Datos de usuario',
                    users_data: result
                })
            } else {
                res.status(404).json({
                    title: 'El usuario no existe'
                })
            }
        }).catch(err => {
            res.status(404).json({
                title: 'Error al listar usuario',
                error: err
            })
        })
};

//ELIMINAR USUARIO
const delUser = async (req, res) => {
    await Users.destroy({
        where: {
            id: req.params.id,
        }
    }).then(
        res.status(201).json({
            title: `Usuario ${req.params.id} eliminado correctamente`,
        })
    ).catch(err => {
        res.status(400).json({
            title: 'Ocurrió un error',
            error: err
        })
    })
};

//MODIFICAR USUARIO
const modUser = async (req, res) => {
    const { user, password, name, phone, email, admin } = req.body;
    const mod_user = await Users.update({
        user: user,
        password: password,
        name: name,
        phone: phone,
        email: email,
        isAdmin: admin
    }, {
        where: {
            id: req.params.id
        }
    })
        .then(
            res.status(201).json({
                title: 'Usuario modificado correctamente',
            }))
        .catch(err => {
            res.status(400).json({
                title: 'Ocurrió un error',
                error: err
            })
        })
};

//BLOQUEAR USUARIO
const banUser = async (req, res) => {
    await Users.update({
        banned: '1'
    }, {
        where: {
            id: req.params.id
        }
    })
        .then(
            res.status(201).json({
                title: 'Usuario bloqueado correctamente',
            }))
        .catch(err => {
            res.status(400).json({
                title: 'Ocurrió un error',
                error: err
            })
        })
};

//DESBLOQUEAR USUARIO
const unbanUser = async (req, res) => {
    await Users.update({
        banned: '0'
    }, {
        where: {
            id: req.params.id
        }
    })
        .then(
            res.status(201).json({
                title: 'Usuario desbloqueado correctamente',
            }))
        .catch(err => {
            res.status(400).json({
                title: 'Ocurrió un error',
                error: err
            })
        })
};

//LISTAR DIRECCIONES DE UN USUARIO POR ID
const getUserAddresses = async (req, res) => {
    await Addresses.findAll({
        where: {
            UserId: req.params.id
        }
    })
        .then(result => {
            if (result == 0) {
                res.status(404).json({
                    title: 'El ID de usuario no existe'
                })
            } else {
                res.status(201).json({
                    title: 'Datos de usuario',
                    users_data: result
                })
            }
        }).catch(err => {
            res.status(404).json({
                title: 'Error al listar direcciones de usuario',
                error: err
            })
        })
};

module.exports = {
    allUsers,
    getUser,
    delUser,
    modUser,
    banUser,
    unbanUser,
    getUserAddresses
};