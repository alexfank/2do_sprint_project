const passport = require('passport');
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;

function prepareStrategy() {
    passport.use(new LinkedInStrategy({
        clientID: process.env.LINKEDIN_KEY,
        clientSecret: process.env.LINKEDIN_SECRET,
        callbackURL: process.env.LINKEDIN_CALLBACK,
        scope: ['r_emailaddress', 'r_liteprofile'],
        state: false
    }, function (accessToken, refreshToken, profile, done) {
        console.log(accessToken); // PRIVADO
        console.log(refreshToken); // PRIVADO
        console.log(profile); // PRIVADO
        return done(null, profile);
    })
    )
}

module.exports = prepareStrategy;

