const redis = require('redis');
const config = require('../config')

//CONECTO CON REDIS
const client = redis.createClient({
  host: config.redis.host,
  port: config.redis.port
});

//VERIFICO CONEXIÓN
client.on('error', function (error) {
  console.error('Error al conectar con Redis', error);
});

client.on('connect', () => {
  console.log('Conectado correctamente con Redis por el puerto ' + config.redis.port);
});

module.exports = { client }