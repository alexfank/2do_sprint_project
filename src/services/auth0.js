const { auth } = require('express-openid-connect');

function initializeAuth0(app) {
  const config = {
    authRequired: false,
    auth0Logout: true,
    baseURL: process.env.AUTH0_BASEURL,
    clientID: process.env.AUTH0_CLIENTID,
    issuerBaseURL: process.env.AUTH0_ISSUERBASEURL,
    secret: process.env.AUTH0_SECRET,
    // routes: {}
  };
  console.log({
    ...config,
    secret: `1`
  })
  app.use(auth(config));
}

module.exports = {
  initializeAuth0
}
