const passport = require('passport');
const GithubStrategy = require('passport-github').Strategy;

function prepareStrategy() {
  passport.use(new GithubStrategy({
    clientID: process.env.GITHUB_CLIENT_ID,
    clientSecret: process.env.GITHUB_SECRET,
    callbackURL: process.env.GITHUB_CALLBACK
  },
    function (accessToken, refreshToken, profile, cb) {
      console.log(accessToken); //PRIVADO
      console.log(refreshToken); //PRIVADO
      console.log(profile); //PRIVADO
      return cb(null, profile);
    },
    function (err, cb) {
      return cb(err)
    }))
};

module.exports = prepareStrategy;