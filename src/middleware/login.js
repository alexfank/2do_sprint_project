const { Users, Addresses } = require('../database/sync')

const middlewares = {

emptyFields: (req,res,next) => {
    const {user, password, name, email, phone, address} = req.body;
    switch(req.url){
        case '/new':
            if(!user || !password || !name || !email || !phone || !address){
                res.status(400).json({
                    title: 'Debe completar todos los campos'
                })
            }else{
                next();
            }
            break;
        case '/':
            if(!user || !password){
                res.status(400).json({
                    title: 'Debe completar todos los campos'
                })
            }else{
                next();
            }
            break;
    }
},

duplicateUser: async(req,res,next) => {
    let userDup = await Users.findOne({
        where: {
            user: req.body.user
        }
    })
    if(userDup){
        return res.status(403).send('El usuario ya existe')
    }else{
        next();
    }
},

duplicateEmail: async (req,res,next) => {
    let emailDup = await Users.findOne({
        where:{
            email: req.body.email
        }
    })
    if(emailDup){
        return res.status(403).send('El email ya existe')
    }else{
        next();
    }
},

}

module.exports = middlewares;