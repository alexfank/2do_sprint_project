const { Orders, Products } = require("../database/sync");

const middlewares = {

    orderValidation: async (req, res, next) => {
        const orderVal = await Orders.findOne({ where: { id: req.params.id } });
        if (!orderVal) {
            return res.status(400).json({msg: 'El ID de la orden no existe'});
        }
        next();
    },
    productValidation: async (req, res, next) => {
        const productVal = await Products.findOne({ where: { id: req.body.productID } });
        if (!productVal) {
            return res.status(400).json({msg: 'El ID del producto no existe'});
        }
        next();
    },
    orderClosed: async (req, res, next) => {
        await Orders.findOne({ where: { id: req.params.id } })
        .then(data =>{
            if(!data){
                return res.status(403).json({msg: 'La orden ingresada no existe.'});
            }else{
                if (data.status != 'Pendiente') {
                    return res.status(403).json({msg: 'Ya no puede modificar la orden ya que no se encuentra abierta.'});
                }
            }
            next();
        })
    },
}

module.exports = middlewares;