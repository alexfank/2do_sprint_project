const { PayMethods } = require("../database/sync");

const middlewares = {

    emptyFields: (req,res,next) => {
        const name = req.body.name;
        if(!name){
            res.status(400).json({
                title: 'Debe completar todos los campos'
            })
        }else{
            next();
        }
    },
    duplicatePayMethod: async(req,res,next) => {
        let payDup = await PayMethods.findOne({
            where: {
                name: req.body.name
            }
        })
        if(payDup){
            return res.status(400).json({
                title: 'El método de pago ya existe'
            })
        }else{
            next();
        }
    },
    payExist: async(req,res,next) => {
        let payMethod = await PayMethods.findOne({
            where: {
                id: req.params.id
            }
        })
        if(!payMethod){
            return res.status(400).json({
                title: 'El ID del método de pago no existe'
            })
        }else{
            next();
        }
    },
}

module.exports = middlewares;