const { Products } = require("../database/sync");

const middlewares = {

    emptyFields: (req,res,next) => {
        const {name, price} = req.body;
        if(!name || !price){
            res.status(400).json({
                title: 'Debe completar todos los campos'
            })
        }else{
            next();
        }
    },
    duplicateProduct: async(req,res,next) => {
        let productDup = await Products.findOne({
            where: {
                name: req.body.name
            }
        })
        if(productDup){
            return res.status(403).json({
                title: 'El producto ya existe'
            })
        }else{
            next();
        }
    },
    productExist: async(req,res,next) => {
        let product = await Products.findOne({
            where: {
                id: req.params.id
            }
        })
        if(!product){
            return res.status(400).json({
                title: 'El ID del producto no existe'
            })
        }else{
            next();
        }
    },
}

module.exports = middlewares;