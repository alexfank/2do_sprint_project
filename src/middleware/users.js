const { Users, Addresses } = require('../database/sync')
const config = require('../config')
const jwt = require('jsonwebtoken')

const middlewares = {

    userExist: async (req, res, next) => {
        let userExist = await Users.findOne({ where: { id: req.params.id } })
        if (!userExist) {
            res.status(400).json('El ID del usuario no existe')
        } else {
            next();
        }
    },

    duplicateEmail: (req, res, next) => {
        let emailDup = users.find(email => req.body.email == email.email);
        if (emailDup) {
            return res.status(400).send('El email ya existe')
        };
        next();
    },

    isAdmin: async (req, res, next) => {
        await Users.findOne({
            where: {
                isLogged: '1',
            }
        })
        .then(data => {
            if(data.isAdmin == 0){
                    return res.status(401).json({
                        msg: 'El usuario no tiene privilegios de administrador'
                    })
                }
                next();
            }
        )
    },

    isLogged: async (req, res, next) => {
        await Users.findOne({
            where: {
                isLogged: '1'
            }
        }).then(data => {
            if (!data) {
                return res.status(401).json({msg: 'Usuario no logueado'});
            } 
            next();
        })
    },

    verifyToken: async (req, res, next) => {
        await Users.findOne({
            where: {
                isLogged: '1'
            }
        }).then(data => {
            try {
                verifyToken = jwt.verify(data.token, config.jwt.secret);
                if(verifyToken){
                    return next();
                } 
            } catch (err) {
                res.status(401).json({error: 'Error al validar el usuario: ', err});
            }
        })
    },

}

module.exports = middlewares;
