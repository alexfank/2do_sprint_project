const express = require('express');
const router = express.Router();
const mercadopago = require('./mercadopago');


function prepareRoutes() {
  router.get('/failed', (req, res) => res.send('You Failed to log in!'))
  router.use(mercadopago());
  return router

}

module.exports = prepareRoutes;