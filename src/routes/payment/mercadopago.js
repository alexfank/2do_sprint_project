// SDK de Mercado Pago
const mercadopago = require('mercadopago');
const express = require('express');
const router = express.Router();

function preparePaymentRouter() {
    const mercadopago = require('mercadopago');
    const config = require('../../config').config


    router.get('/payment/token', (req, res) => {
        res.json({
            token: process.env.MERCADOPAGO_PUBLIC_KEY
        })
    })

    router.post('/process_payment', (req, res) => {
        mercadopago.configurations.setAccessToken(process.env.MERCADOPAGO_ACCESS_TOKEN);
        mercadopago.payment.save(req.body)
            .then(function (response) {
                const { status, status_detail, id } = response.body;
                res.status(response.status).json({ status, status_detail, id });
            })
            .catch(function (error) {
                console.error(error);
                res.status(406).json({ status: 406, status_detail: 'Error on payment' })
            });
    });
    return router;
}

module.exports = preparePaymentRouter;