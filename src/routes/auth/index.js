const express = require('express');
const router = express.Router();
const google = require('./google');
const facebook = require('./facebook');
const linkedin = require('./linkedin');
const github = require('./github');

function prepareRoutes() {
  router.get('/failed', (req, res) => res.send('Ocurrió un fallo en el login!'))
  router.use(google());
  router.use(facebook());
  router.use(linkedin());
  router.use(github());
  return router

}

module.exports = prepareRoutes;
