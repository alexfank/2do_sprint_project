const express = require('express');
const passport = require('passport');

function prepareRouter() {
  const strategy_name = 'linkedin';
  const router = express.Router();
  router.get('/linkedin/auth', passport.authenticate(strategy_name, {
    state: 'SOME STATE'
  }));


  router.get('/linkedin/auth/callback', passport.authenticate(strategy_name, {
    failureRedirect: '/login',
    session: false,
  }),
    function (req, res) {
      const token = 'insert_token'
      res.redirect('/?token=' + token);
    });

  router.get('/login', (req, res) => {
    res.redirect('http://localhost:3000')
  })

  return router;
}

module.exports = prepareRouter;