const express = require('express');
const passport = require('passport');

function prepareRouter() {
  const strategy_name = 'github';
  const router = express.Router();
  router.get('/github/auth', passport.authenticate(strategy_name, {
    session: false
  }));

  router.get('/github/auth/callback',
    passport.authenticate(strategy_name, {
      failureRedirect: '/login',
      session: false
    }),
    function (req, res) {
      const token = 'insert_token'
      res.redirect('/?token=' + token);
    });

  router.get('/login', (req, res) => {
    res.redirect('/')
  })

  return router;
}

module.exports = prepareRouter;