const express = require('express')
const { requiresAuth } = require('express-openid-connect');

function initPublicRouter() {
  const router = express.Router()

  router.get('/authorize', requiresAuth(), (req, res) => {
    const token = 'insert_token'
    res.redirect('/?token=' + token);
  });

  return router;
}

module.exports = initPublicRouter
