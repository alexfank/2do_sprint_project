const router = require('express').Router();

const apiLogin = require('./api/login/');
const apiUsers = require('./api/users/');
const apiProducts = require('./api/products/');
const apiPayMethod = require('./api/payMethod/');
const apiOrders = require('./api/orders/');
const apiOrderDetail = require('./api/orderDetail/');
const apiAddress = require('./api/address/');

router.use('/login', apiLogin);
router.use('/users', apiUsers);
router.use('/products', apiProducts);
router.use('/paymethod', apiPayMethod);
router.use('/orders', apiOrders);
router.use('/orderdetail', apiOrderDetail);
router.use('/address', apiAddress);

module.exports = router;