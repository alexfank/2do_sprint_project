const router = require('express').Router();

router.get('/', async (req, res) => {
    const orderDetail = await OrderDetail.findAll();
    res.json(orderDetail);
});

module.exports = router;