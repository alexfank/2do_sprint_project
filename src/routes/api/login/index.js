const router = require('express').Router();
const controller = require('../../../controllers/login');
const middlewares = require('../../../middleware/login');

router.post('/', middlewares.emptyFields, controller.login);
router.post('/new', middlewares.emptyFields, middlewares.duplicateUser, middlewares.duplicateEmail, controller.newUser);

module.exports = router;