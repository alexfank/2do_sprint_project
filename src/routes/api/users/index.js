const router = require('express').Router();
const controller = require('../../../controllers/users');
const middlewares = require('../../../middleware/users');

router.get('/', middlewares.isLogged, middlewares.isAdmin, controller.allUsers);
router.get('/:id', controller.getUser);
router.delete('/del/:id', middlewares.isLogged, middlewares.isAdmin, middlewares.userExist, controller.delUser);
router.put('/mod/:id', middlewares.isLogged, middlewares.isAdmin, middlewares.userExist, controller.modUser);
router.put('/ban/:id', middlewares.isLogged, middlewares.isAdmin, middlewares.userExist, controller.banUser);
router.put('/unban/:id', middlewares.isLogged, middlewares.isAdmin, middlewares.userExist, controller.unbanUser);
router.get('/addresses/:id', controller.getUserAddresses);

module.exports = router;
