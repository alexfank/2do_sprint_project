const router = require('express').Router();
const controller = require('../../../controllers/payMethod');
const middlewares = require('../../../middleware/payMethod');
const middlAuth = require('../../../middleware/users');

router.get('/', middlAuth.isLogged, controller.allPayMeth);
router.post('/new', middlAuth.isLogged, middlAuth.isAdmin, middlewares.emptyFields, middlewares.duplicatePayMethod, controller.newPayMeth);
router.put('/mod/:id', middlAuth.isLogged, middlAuth.isAdmin, middlewares.payExist, controller.modPayMeth);
router.delete('/del/:id', middlAuth.isLogged, middlAuth.isAdmin, middlewares.payExist, controller.delPayMeth);

module.exports = router;