const router = require('express').Router();
const controller = require('../../../controllers/products');
const middlewares = require('../../../middleware/products');
const middlAuth = require('../../../middleware/users');

router.get('/', controller.allProducts);
router.post('/new', middlAuth.isLogged, middlAuth.isAdmin, middlewares.emptyFields, middlewares.duplicateProduct, controller.newProduct);
router.put('/mod/:id', middlAuth.isLogged, middlAuth.isAdmin, middlewares.productExist, controller.modProduct);
router.delete('/del/:id', middlAuth.verifyToken, middlAuth.isAdmin, middlewares.productExist, controller.delProduct);

module.exports = router;