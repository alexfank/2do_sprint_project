const router = require('express').Router();
const {Address} = require('../../../database/sync');


router.get('/', async (req, res) => {
    const address = await Address.findAll();
    res.json(address);
});

module.exports = router;