const router = require('express').Router();
const controller = require('../../../controllers/orders');
const middleware = require('../../../middleware/orders');
const middlAuth = require('../../../middleware/users');

router.get('/', middlAuth.isLogged, middlAuth.isAdmin, controller.allOrders);
router.post('/new', middlAuth.isLogged, controller.newOrder);
router.post('/add/:id', middlAuth.isLogged, middleware.orderValidation, middleware.orderClosed, controller.addToOrder);
router.put('/mod/:id', middlAuth.isLogged, middlAuth.isAdmin, middleware.orderValidation, controller.modOrder);
router.delete('/del/:id', middlAuth.isLogged, middleware.orderValidation, controller.delOrder);
router.put('/delOne/:id', middlAuth.isLogged, middleware.orderValidation, middleware.productValidation, controller.delFromOrder);

module.exports = router;