const {Sequelize} = require('sequelize');
const {database} = require('../config');

const delilahdb = new Sequelize(database.database, database.username, database.password, {
    host: database.host,
    dialect: database.dialect,
    port: database.port
});

async function validate_conection(){
    try{
        await delilahdb.authenticate();
        console.log('Conectado correctamente a la base de datos.');
    }catch (error){
        console.log('Error al conectarse a la base de datos: ', error);
    }
}

validate_conection();

module.exports = {
    delilahdb
};
