const { Sequelize } = require('sequelize');
const { delilahdb } = require('./db');

//LLAMADA A MODELOS
const UsersModel = require('../models/users');
const ProductsModel = require('../models/products');
const OrdersModel = require('../models/orders');
const OrderDetailsModel = require('../models/orderDetail');
const AddressesModel = require('../models/address');
const PayMethodsModel = require('../models/payMethod');

const Users = UsersModel(delilahdb, Sequelize);
const Products = ProductsModel(delilahdb, Sequelize);
const Orders = OrdersModel(delilahdb, Sequelize);
const OrderDetails = OrderDetailsModel(delilahdb, Sequelize);
const Addresses = AddressesModel(delilahdb, Sequelize);
const PayMethods = PayMethodsModel(delilahdb, Sequelize);

// const bulk = require('./bulk');

//REALIZAR LA SINCRONIZACIÓN Y BULKCREATE
delilahdb.sync({ force: true })
.then(() => {
    console.log('Las tablas fueron sincronizadas.')})
.then(async () => {
    await Users.bulkCreate([
        {
            user: 'admin',
            password: 'admin',
            name: 'Delilah Resto Admin',
            email: 'admin@delilahresto.com',
            isAdmin: 1
        },{
            user: 'alexfank',
            password: '123',
            name: 'Alex Fank',
            email: 'alex@fank.com.ar',
            isAdmin: 0,
            isLogged: 0
        }
    ]);
    await Addresses.bulkCreate([
        {
            address: 'Marcos Zar 1338',
            UserId: 2
        }
    ]);
    await Products.bulkCreate([
        {
            name: "Bagel de Salmón",
            price: 425
        },{
            name: "Hamburguesa Clásica",
            price: 350
        },{
            name: "Sandwich Veggie",
            price: 310
        },{
            name: "Ensalada Veggie",
            price: 340
        },{
            name: "Focaccia",
            price: 300
        },{
            name: "Sandwich Focaccia",
            price: 440
        }
    ]);
    await PayMethods.bulkCreate([
        {
            payID: 1,
            name: "Efectivo"
        },{
            payID: 2,
            name: "VISA Crédito"
        },{
            payID: 3,
            name: "VISA Débito"
        },{
            payID: 4,
            name: "Pay Pal"
        }
    ]);
    console.log('Carga bulk completada')})
.catch(err => { console.log('Ocurrió un error en el sync de las tablas', err) });

module.exports = {
    Users,
    Products,
    Orders,
    OrderDetails,
    Addresses,
    PayMethods,
}
