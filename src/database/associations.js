const { Users, Addresses, Products, Orders, OrderDetails, PayMethods } = require('./sync');

Users.hasMany(Addresses, {
    onDelete: 'cascade'
});
Addresses.belongsTo(Users);

Orders.hasMany(OrderDetails, {
    onDelete: 'cascade'
});
OrderDetails.belongsTo(Orders);