require('dotenv').config();

module.exports = {

    database: {
        username: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE,
        host: process.env.MYSQL_HOST,
        dialect: process.env.MYSQL_DIALECT,
        port: process.env.MYSQL_PORT
    },
    server: {
        port: process.env.SERVER_PORT || 3000
    },
    jwt: {
        secret: process.env.AUTH_SECRET,
        expires: process.env.AUTH_EXPIRES,
    },
    redis: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
    },
    crypto: {
        secret: process.env.CRYPTO_SECRET,
    }
}
