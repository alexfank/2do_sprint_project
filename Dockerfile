FROM node:16.4-alpine

COPY ./ /delilah-resto
WORKDIR /delilah-resto
RUN npm install
CMD npm run dev