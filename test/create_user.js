// TEST DE CREACIÓN DE USUARIO
const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http')

chai.use(chaiHttp);

describe('Test de registro de usuario en la BD', function() {
    it('El usuario completa el registro', (done) => {
        chai.request('http://localhost:3000')
            .post('/api/login/new')
            .send({
            user: 'usuario',
            password: '123456',
            name: 'Usuario Modelo',
            email: 'usuariomodelo@gmail.com',
            phone: '02901422851',
            address: 'Dirección de ejemplo 123'
        })
            .end((err, res) => {
                if(err){
                    throw err
                };
            expect(res.status).equals(400);
            done();
        });
    });
});